help - Output help

       Arguments:
       [command] - Output help for this specific command only (e.g. help init).
                   If not specified, full help message is displayed.
