# DAMP - The Docker Apache MySQL PHP web development stack

Sick and tired of investing time and money into getting a working web development 
environment up and running on your local machine?

You've been searching the internet for a possibility to add this very specific
version of PHP to your development environment, only to find out that it breaks
everything you had setup before?

Worry not, DAMP is here to dampen your pain.

## Purpose

This project aims to provide an easy to start web development environment based on
Docker letting you focus on your project instead of having to put plenty of time
into setting up your local development environment because the production server
runs a very specific version of PHP which you don't have on your local machine
or which would break your locally installed PHP versions etc.

Instead, you can now simply add the DAMP package to your project - you are using 
[composer](https://getcomposer.org/) aren't you? - configure the settings in a dotenv 
file and start the Docker containers.

As a bonus you get a [MailHog](https://github.com/mailhog/MailHog) container that 
allows you to test all email based things in your project. No need to worry about
setting up an MTA or being afraid of sending emails to actual recipients, MailHog
catches all emails sent via PHP's `mail()` function.


## Install

To install DAMP in your project, add it as a development requirement:

```shell
composer require --dev troggybrains/damp
```

After installing the package, initialize DAMP in your project:

```shell
./vendor/bin/damp init
```

This will create a symbolic link called `docker-compose.yml` and a file called
`.env` in your project root. In case you already had a `.env` in your project root,
you'll have to append the contents of `vendor/troggybrains/damp/.env.dist` to
that file:

```shell
cat vendor/troggybrains/damp/.env.dist >>./.env
```

Additionally the folder `damp.d` will be created in your project root. This folder 
is used by DAMP to let you store arbitrary data for your [services](#services).

## Configure

Now that you have DAMP installed, all that is left to do, is adapt the variables 
inside `.env` to your needs. All variables are documented inside `.env` so you
don't have to read some documentation in parallel.

Let me explain the most important variables here though.

* `PROJECT_NAME`  
   Defines the name of your project and subsequently of the containers that are
   built for your project. It is important so set this to a unique value, otherwise
   your projects will attempt to share containers which will finally lead to issues.
* `PHP_VERSION`  
   Sets the PHP version that your project will run on. This is the real strength and 
   purpose of DAMP. Have a look at the available [PHP Docker images](https://hub.docker.com/_/php?tab=tags&page=1&ordering=last_updated&name=-apache)
   to check which versions you can deal with.  
   Note that only image tags ending on `-apache` are supported by DAMP.
* `MYSQL_VERSION`  
   If you can chose the PHP version to run, you should also be able to chose the MySQL 
   version to run on. This is what this variable stands for.  
   Have a look at the [MySQL Docker images](https://hub.docker.com/_/mysql?tab=tags&page=1&ordering=last_updated)
   to check which versions you can use.

When you're done, add `docker-compose.yml` and `.env` to your VCS, so that your 
colleagues can benefit from the setup.

You will probably want to access your project via a domain (e.g. `http://mypropject.test/`). 
Since DAMP does not come with a nameserver of some sorts, you will have to take care 
about resolving domain names yourself. This is quite easy to accomplish though, simply
append a line similar to the following to your local `/etc/hosts`:

```
127.0.0.1   myproject.test www.myproject.test
```

Save the file and that's it, no need to restart any services.

## Running

As soon as you have configured the variables in `.env`, you're ready to fire up DAMP.

Unless you have set `HTTPD_PORT` to a custom value, the web server binds to port
80 of your machine, thus you have to stop services that are already bound to that
port (e.g. Apache, NginX, lighttpd) before starting DAMP.

As of now, the web server container is designed to catch all requests directed at 
`HTTPD_PORT`, thus running multiple project environments in parallel is not 
possible yet. This will be implemented soon though, along with support for HTTPS.

### Starting

To start DAMP simply issue the following command:

```shell
./vendor/bin/damp start
```

Note that since this may have to pull the appropriate docker images, which make 
roughly 1Gb in total, starting DAMP for the first time may take a while.

### Stopping

To stop DAMP issue the following command:

```shell
./vendor/bin/damp stop
```

### Convenience

For convenience purposes and since the path to the `vendor` and `bin` folders are configurable,
we suggest to add starting and stopping as [custom commands](https://getcomposer.org/doc/articles/scripts.md#writing-custom-commands).  

In order to do so, add the following lines to your `composer.json`:

```json
{
    ...,
    "scripts": {
        "damp:start": "damp start",
        "damp:stop": "damp stop"
    }

}
```

These can the be executed by issuing `composer damp:start` and `composer damp:stop` on the command line.
Composer will take care of finding and executing the appropriate `damp` binary.

The sample `scripts` block is displayed when executing `./vendor/bin/damp composer`, thus allowing to add 
the scripts programatically.

## Services

### web

The `web` service expects to find the folder `src` in your project root, which
will be mounted as the document root inside the service. If the folder does not
exist when DAMP is started, it will automatically be created.

Unfortunately it will then be owned by `root` and you won't be able to write to
the folder, thus you should make sure that the folder exists before you start DAMP.
In case you started DAMP before creating the folder, you'll have to stop DAMP and
change ownership of the folder `src`.

This service has the following PHP extensions preinstalled and enabled:

* intl
* gd
* mysqli
* soap
* xdebug

You can configure which Xdebug major version (2.x or 3.x) the service should have.
Note though, that setting `XDEBUG_MAJOR_VERSION` to '2' will not have any effect
if `PHP_VERSION` is set to anything greater or equal PHP 8.x as the latter is not 
supported by Xdebug 2.x.

By default Xdebug is enabled and configured to connect back, see section [Debugging](#debugging)
for more information.

This service uses the folder `damp.d/web` to let you customize your exeperience.

**Files:**

* `php/project.ini`  
   This file is mounted into the configuration folder of PHP inside the service,
   thus allowing you customize PHP settings at will. DAMP makes sure that this 
   file is loaded last, thus making sure that your settings overwrite all previous
   settings.

### db

The `db` service uses the following folders, that should have been created by the
`init` command, in your project root:

* `damp.d/db/dump`  
   In case you want DAMP to automatically populate your project database, this is 
   where you can place dump files. See the section "Initializing a fresh instance"
   of the official [MySQL Docker image](https://hub.docker.com/_/mysql) documentation
   for more information on how this mechanism works.
* `damp.d/db/mysql`  
   This is where MySQL database files will be stored. The reason they are stored
   in a mounted folder is that they should be persistent even when the containers 
   are deleted (e.g. `vendor/bin/damp stop -d`). It is strongly advised to have
   this folder ignored by your VCS, as the content of this folder can be quite large.

In case you want to access the database using a GUI of yome sorts, this service binds 
to a configurable port on your machine.

### mail

This services as an MTA for your project and automatically catches all e-mails that are
sent via PHP's [`mail()`](https://www.php.net/manual/en/function.mail.php).

It comes with a web frontend in which you can have a look at the emails, which is
accessible via port 8025 of your machine, e.g. http://localhost:8025/.

In case you have to configure an SMTP server in your project, here is what you need:

* Host: mail
* Port: 1025

Note that this will work from the `web` service only, external devices cannot connect 
to the SMTP running inside this service.

## Debugging

As stated before, Xdebug is preinstalled, enabled and configured to connect back to the 
client that called it.

Thus starting a debug session is just a matter of setting a query parameter or a cookie,
as described [here](https://xdebug.org/docs/step_debug#activate_debugger).

Manually setting the query parameter / cookie can become quite a hassle, especially when
attemting to step debug AJAX calls, thus you should consider installing a browser plugin
that will perform this for you (as described [here](https://xdebug.org/docs/step_debug#browser-extensions)).

Look up your IDEs documentation for detailed information on how to set up remote debugging.

## Tools

tbd

## TODO

1. Create project domain only catch
1. Add HTTPS support
1. Improve documentation
