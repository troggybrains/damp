#!/bin/sh
#
# Configures and installs PHP GD with Freetype and JPEG support.
# This script works around the GD issue reported on GitHub (https://github.com/docker-library/php/issues/865).
#

PHP_VERSION=$(php -r "echo PHP_MAJOR_VERSION . PHP_MINOR_VERSION;");
GD_CONFIG_ARGS='';

if [ "${PHP_VERSION}" -lt 74 ]; then
    GD_CONFIG_ARGS="--with-freetype-dir=/usr/include/freetype --with-jpeg-dir=/usr/include";
else
    GD_CONFIG_ARGS="--with-freetype --with-jpeg";
fi

docker-php-ext-configure gd ${GD_CONFIG_ARGS};
docker-php-ext-install -j$(nproc) gd;
