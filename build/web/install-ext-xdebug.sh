#!/bin/sh
#
# Installs and enables PHP Xdebug.
# This script works around the problem that PHP >= 8.0 is supported by Xdebug 3.x while
# lesser PHP versions may want to use Xdebug 2.x for compatibility reasons.
# Additionally this allows users to define whether they want to force Xdebug 3.x in their image,
# even with PHP versions lesser than 8.0.
#

PHP_MAJOR_VERSION=$(php -r "echo PHP_MAJOR_VERSION;");
XDEBUG_MAJOR_VERSION=3;
XDEBUG_PACKAGE='xdebug';

if [ -n "$1" ]; then
    XDEBUG_MAJOR_VERSION=$1;
fi

if [ "${PHP_MAJOR_VERSION}" -lt 8 -a "${XDEBUG_MAJOR_VERSION}" -ne 3 ]; then
    XDEBUG_PACKAGE="${XDEBUG_PACKAGE}-2.8.1";
fi

pecl install ${XDEBUG_PACKAGE};
docker-php-ext-enable xdebug;
